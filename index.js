/*
*   $$\      $$\                                                $$$$$$\  $$$$$$$\ $$$$$$\ 
*   $$$\    $$$ |                                              $$  __$$\ $$  __$$\\_$$  _|
*   $$$$\  $$$$ | $$$$$$\  $$$$$$$\   $$$$$$\   $$$$$$\        $$ /  $$ |$$ |  $$ | $$ |  
*   $$\$$\$$ $$ | \____$$\ $$  __$$\ $$  __$$\  \____$$\       $$$$$$$$ |$$$$$$$  | $$ |  
*   $$ \$$$  $$ | $$$$$$$ |$$ |  $$ |$$ /  $$ | $$$$$$$ |      $$  __$$ |$$  ____/  $$ |  
*   $$ |\$  /$$ |$$  __$$ |$$ |  $$ |$$ |  $$ |$$  __$$ |      $$ |  $$ |$$ |       $$ |  
*   $$ | \_/ $$ |\$$$$$$$ |$$ |  $$ |\$$$$$$$ |\$$$$$$$ |      $$ |  $$ |$$ |     $$$$$$\ 
*   \__|     \__| \_______|\__|  \__| \____$$ | \_______|      \__|  \__|\__|     \______|
*                                    $$\   $$ |                                           
*                                    \$$$$$$  |                                           
*                                     \______/                                            
**/ 

/**
 *  PLEASE NOTE TO ADD A CACHE OR YOU CAN GET 
 *  IP BANNED FROM THE SITE
*/


const { default: axios } = require("axios")
const cheerio = require("cheerio")
const express = require("express")
const scraper = require("./scrarper")

const PORT = process.env.PORT | 9121
const app = express()

app.get("/", (req, res) => {
    res.json({
        msg: "Thank you for using me"
    })
})

app.get('/search/info/:NAME', async (req, res) => {
    let URI = `https://ww3.mangakakalot.tv/search/${req.params.NAME}`

    axios.get(URI).then((html) => {                
        let MangaInfo = []
        let $ = cheerio.load(html.data)

            $("div.story_item").each((i, el) => {
                let anime = {}    

                anime.id = $(el).find("h3.story_name a").attr("href").replace("/manga/manga-", "")
                anime.title = $(el).find("h3.story_name a").text()
                anime.author = $(el).find("div.story_item_right span").html().replace(/\s\s+/g, " ")
                anime.links = `https://ww3.mangakakalot.tv${$(el).find("h3.story_name a").attr("href")}`
                anime.image = $(el).find("a img").attr("src")
                anime.last_chapter = $(el).find("em.story_chapter a").html().replace(/\s\s+/g, " ")
                anime.last_updated = $(el).find("div.story_item_right span").next().html().replace("Last update :", "").trim()
                anime.views = $(el).find("div.story_item_right span").next().next().html().replace("Views :", "").trim()
                
                MangaInfo.push(anime)
            })

        res.json(MangaInfo)
    })
})

app.get("/search/chapter/:ID", (req, res) => {
    let MangURL = `https://ww3.mangakakalot.tv/manga/manga-${req.params.ID}`

    axios.get(MangURL).then((response) => {
        let $ = cheerio.load(response.data)        
        
        let MangaChapter = {}
        const MangaChapterItem = []
        
        MangaChapter.$status = $("ul.manga-info-text li").next().next().html().replace("Status :", "").trim()
        MangaChapter.$last_update = $("ul.manga-info-text li").next().next().next().html().replace("Last updated :", "").trim()
        MangaChapter.$views = $("body > div.container > div.main-wrapper > div.leftCol > div.manga-info-top > ul > li:nth-child(6)").html().replace("View : ", "").trim()

        MangaChapter.$author = $("body > div.container > div.main-wrapper > div.leftCol > div.manga-info-top > ul > li:nth-child(2) a").text()

        $("div.row span").children().each((i, el) => {
            MangaChapter.$chapter = $(el).text().trim()  
            
            $("div.chapter-list div.row span a").each((i, els) => {
                MangaChapter.$link = `${MangURL.replace(`/manga/manga-${req.params.ID}`, "")}${$(el).attr("href")}`
            })


            $("div.row").next().find("span").next().each((i, elm) => {
                MangaChapter.$date = $(elm).html().trim()
            })

            MangaChapterItem.push(MangaChapter)
        })

        res.json(MangaChapterItem)
    }) 
})

app.get("/search/images", (req, res) => {
    axios.get(req.query.link).then((response) => {
        let $ = cheerio.load(response.data)
        
        let data = []
        let $chapter_on = $("div.info-top-chapter h2").html().trim().replace(/\s\s/g, "")

        $("img.img-loading").each((i, el) => {
            let $img_link = $(el).attr("data-src")
            let $img_alt = $(el).attr("alt")

            let ChapterImg = {
                $chapter_on,
                $img_link,
                $img_alt
            }

            data.push(ChapterImg)
        })

        res.json(data)
    })
})

app.listen(PORT, function() {
    console.log('Listening on port %d', PORT);
});
